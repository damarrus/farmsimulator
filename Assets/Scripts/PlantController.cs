using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantController : MonoBehaviour
{
    [SerializeField] private string plantName;
    [SerializeField] private float timeToGrow;
    [SerializeField] private float yStart;
    [SerializeField] private float yEnd;

    [SerializeField] public int reward;

    private IEnumerator coroutine;

    public ItemType PlantType;
    public bool HasGrown;
    public event Action OnGrowth;


    void Start() {
        transform.position = new Vector3(transform.position.x, yStart, transform.position.z);
    }

    void Update() {
        
    }

    public void StartGrowing() {
        if (coroutine == null) {
            coroutine = GrowRoutine(yStart, yEnd, timeToGrow, 3);
            StartCoroutine(coroutine);
        }
    }

    private IEnumerator GrowRoutine(float yStart, float yEnd, float time, int count) {
        var growPerCount = (yEnd - yStart) / count;
        var timePerCount = time / count;

        for (int i = 0; i < count; i++) {
            yield return new WaitForSeconds(timePerCount);

            var endCurrentGrow = yStart + growPerCount * (i + 1);
            var startCurrentGrow = transform.position.y;
            for (int j = 1; j <= 60; j++) {

                var intervalGrow = Mathf.Lerp(startCurrentGrow, endCurrentGrow, j / 60f);

                transform.position = new Vector3(transform.position.x, intervalGrow, transform.position.z);
                yield return new WaitForFixedUpdate();
            }
        }

        HasGrown = true;
        OnGrowth?.Invoke();
    }
}
