using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    [SerializeField] private PlayerInputController playerInputController;
    [SerializeField] private InstrumentController instrumentController;
    [SerializeField] private TargetController targetController;
    [SerializeField] private PanelController panelController;

    [SerializeField] private GameObject rightHand;
    [SerializeField] private GameObject leftHand;

    [SerializeField] private GameObject shovel;
    [SerializeField] private GameObject sickle;
    [SerializeField] private GameObject axe;
    [SerializeField] private GameObject bucket;

    [SerializeField] private GameObject villager;

    [SerializeField] private Rigidbody rigid;
    [SerializeField] private float speed;

    [SerializeField] private Animator animator;

    private static int SPEED = Animator.StringToHash("speed");
    private static int PLOW = Animator.StringToHash("plow");
    private static int CHOP = Animator.StringToHash("chop");
    private static int CUT = Animator.StringToHash("cut");
    private static int COLLECT = Animator.StringToHash("collect");
    private static int WATER = Animator.StringToHash("water");
    private static int PLANT = Animator.StringToHash("plant");

    private IEnumerator coroutine;
    private PendingAction pendingAction;
    private Vector3 currentCellPosition;

    private GameObject selectedInstrument;

    public bool isBusy;
    public event Action<Vector3> OnMove;
    public event Action<ItemType> OnCollect;

    void Start() {
        playerInputController.OnMoveInput += MoveIfPossible;
        panelController.OnSlotChange += EquipInstrument;

        EquipInstrument(ItemType.SHOVEL);
    }

    void Update() {

    }

    private void EquipInstrument(ItemType itemType) {
        GameObject instrument = null;

        switch (itemType) {
            case ItemType.SHOVEL: instrument = shovel; break;
            case ItemType.BUCKET: instrument = bucket; break;
            case ItemType.SICKLE: instrument = sickle; break;
            case ItemType.AXE: instrument = axe; break;
        }

        if (selectedInstrument != null) selectedInstrument.SetActive(false);

        if (instrument != null) {
            selectedInstrument = instrument;
            selectedInstrument.SetActive(true);
        } 
    }

    public void StartAnimation(Action callback, ItemType itemType, CellController targetCell) {
        var cellPosition = targetCell.transform.position;
        if (isBusy) {
            if (currentCellPosition == cellPosition) return;

            pendingAction = new PendingAction(callback, itemType, targetCell);
            return;
        }

        isBusy = true;
        currentCellPosition = cellPosition;
        coroutine = AnimationRoutine(callback, itemType, cellPosition);
        StartCoroutine(coroutine);
    }

    private IEnumerator AnimationRoutine(Action callback, ItemType itemType, Vector3 cellPosition) {
        
        int animation = 0;
        float animationTime = 1.25f;

        switch (itemType) {
            case ItemType.EMPTY:
            case ItemType.HAND: 
                animation = COLLECT; 
                break;
            case ItemType.SHOVEL: 
                animation = PLOW;
                animationTime = 1.5f;
                break;
            case ItemType.BUCKET: 
                animation = WATER; 
                break;
            case ItemType.FERTILIZERS: 
                animation = PLOW; 
                break;
            case ItemType.SICKLE: 
                animation = CUT; 
                break;
            case ItemType.AXE: 
                animation = CHOP;
                animationTime = 6;
                break;
            case ItemType.CARROT: 
            case ItemType.POTATO: 
            case ItemType.BEET: 
                animation = PLANT; 
                break;
        }

        if (animation != 0) {
            animator.SetFloat(SPEED, 0);
            animator.SetBool(animation, true);
            villager.transform.rotation = Quaternion.LookRotation(cellPosition - villager.transform.position);

            yield return new WaitForSeconds(animationTime);
            villager.transform.position = new Vector3(transform.position.x, 0, transform.position.z);
            animator.SetBool(animation, false);
            isBusy = false;
            callback();

            if (pendingAction != null) {
                yield return new WaitForFixedUpdate();
                if (targetController.CellAction(pendingAction.targetCell)) {
                    StartAnimation(pendingAction.callback, pendingAction.itemType, pendingAction.targetCell);
                }
                pendingAction = null;
            }
        }
    }

    private void MoveIfPossible(Vector3 direction) {

        if (!isBusy) {
            // ������������
            rigid.velocity = direction * speed;
            animator.SetFloat(SPEED, rigid.velocity.sqrMagnitude);

            OnMove.Invoke(transform.position);

            // ���� ��������
            Quaternion newRotation = Quaternion.LookRotation(direction);
            villager.transform.rotation = Quaternion.Slerp(villager.transform.rotation, newRotation, Time.deltaTime * 8);
        } else if (pendingAction != null) {
            pendingAction = null;
        }
    }

    private void OnDrawGizmos() {

        //Gizmos.color = Color.red;
        //Gizmos.DrawLine(new Vector3(0, 3, 0), new Vector3(1, 3, 1));
        //Gizmos.DrawLine(new Vector3(1, 3, 1), new Vector3(1, 3, 0));
        //Gizmos.DrawLine(new Vector3(1, 3, 0), new Vector3(0, 3, 0));
        

    }
}
