using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TasksController : MonoBehaviour
{
    [SerializeField] private GoldController goldController;

    [SerializeField] private TextMeshProUGUI carrotTask;
    [SerializeField] private TextMeshProUGUI potatoTask;
    [SerializeField] private TextMeshProUGUI beetTask;
    [SerializeField] private TextMeshProUGUI goldTask;

    [SerializeField] private int carrotTaskCount;
    [SerializeField] private int potatoTaskCount;
    [SerializeField] private int beetTaskCount;
    [SerializeField] private int goldTaskCount;

    private int carrotCurrentCount = -1;
    private int potatoCurrentCount = -1;
    private int beetCurrentCount = -1;

    void Start()
    {
        goldController.OnChangeGold += SetGoldTask;
        SetGoldTask(0);
        SetPlantsTasks(ItemType.CARROT);
        SetPlantsTasks(ItemType.POTATO);
        SetPlantsTasks(ItemType.BEET);
    }

    public void SetPlantsTasks(ItemType itemType) {
        switch (itemType) {
            case ItemType.CARROT:
                carrotTask.text = $"��������� �������� ({++carrotCurrentCount}/{carrotTaskCount})";
                break;
            case ItemType.POTATO:
                potatoTask.text = $"��������� �������� ({++potatoCurrentCount}/{potatoTaskCount})";
                break;
            case ItemType.BEET:
                beetTask.text = $"��������� ������� ({++beetCurrentCount}/{beetTaskCount})";
                break;
        }
    }

    private void SetGoldTask(int gold) {
        goldTask.text = $"�������� ������ ({gold}/{goldTaskCount})";
    }
}
