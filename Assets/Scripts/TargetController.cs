using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetController : MonoBehaviour
{
    [SerializeField] private PlayerInputController playerInputController;
    [SerializeField] private PlayerController playerController;
    [SerializeField] private TerrainController terrainController;
    [SerializeField] private PanelController panelController;
    [SerializeField] private ErrorController errorController;
    [SerializeField] private GoldController goldController;

    [SerializeField] private Color greenTargetColor;
    [SerializeField] private Color redTargetColor;
    [SerializeField] private Color grayTargetColor;

    public CellController targetCell { get; private set; }
    private CellController playerCell;

    private Renderer targetRenderer;

    public bool CanAction;

    IEnumerator Start()
    {
        // ���� �������� TargetController
        yield return null;

        targetRenderer = GetComponent<Renderer>();
        playerInputController.OnRaycastToCell += SetTargetToCell;
        playerController.OnMove += ChangePlayerCell;
        ChangePlayerCell(playerController.transform.position);
    }

    private void ChangePlayerCell(Vector3 playerPosition) {
        playerCell = terrainController.GetCell(playerPosition);
    }

    void Update()
    {
        
    }

    private void SetTargetToCell(RaycastHit hit) {

        if (hit.collider != null && hit.collider.gameObject != targetCell) {
            targetCell = hit.collider.gameObject.GetComponent<CellController>();
            transform.position = targetCell.transform.position;
        }

        CellAction(targetCell);

    }

    public bool CellAction(CellController targetCell) {
        List<ErrorType> errors = new List<ErrorType>();
        var result = false;
        CanAction = false;
        if (targetCell != null && playerCell != null && Vector3.Distance(playerCell.transform.position, targetCell.transform.position) < 3) {

            var itemType = panelController.NextSlot != null ? panelController.NextSlot.type : panelController.SelectedSlot.type;

            if (CanCollect()) {
                result = true;
                errors.Add(ErrorType.CAN_COLLECT);
            } else {
                switch (itemType) {
                    case ItemType.SHOVEL:
                        result = CanPlow(out errors);
                        break;
                    case ItemType.BUCKET:
                        result = CanToWaterPatch(out errors);
                        break;
                    case ItemType.FERTILIZERS:
                        break;
                    case ItemType.SICKLE:
                        result = CanCut(out errors);
                        break;
                    case ItemType.AXE:
                        result = CanChop(out errors);
                        break;
                    case ItemType.HAND:
                        result = CanClean(out errors);
                        break;
                    case ItemType.CARROT:
                    case ItemType.POTATO:
                    case ItemType.BEET:
                        result = CanPlant(itemType, out errors);
                        break;
                }
            }

            if (targetCell.IsPlowed && !targetCell.IsWatered) {
                errors.Add(ErrorType.NOT_IS_WATERED);
            }


            if (result) {
                targetRenderer.material.SetColor("_Color", greenTargetColor);
                CanAction = true;
            } else {
                targetRenderer.material.SetColor("_Color", redTargetColor);
                CanAction = false;
            }

        } else {
            targetRenderer.material.SetColor("_Color", grayTargetColor);
            CanAction = false;

            errors.Add(ErrorType.TOO_FAR);
        }

        errorController.Show(errors);

        return CanAction;
    }

    public bool CanCollect() {
        return targetCell.PlantObject != null && targetCell.PlantObject.HasGrown;
    }

    private bool CanPlow(out List<ErrorType> errors) {
        errors = new List<ErrorType>();

        if (HasBlocksPlow()) errors.Add(ErrorType.CANT_PLANT);
        if (targetCell.Type != CellObjectType.EMPTY) errors.Add(ErrorType.NOT_IS_EMPTY);
        if (targetCell.IsPlowed) errors.Add(ErrorType.IS_PLOWED);

        return errors.Count == 0;
    }

    private bool HasBlocksPlow() {

        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                if (terrainController.cells[targetCell.X + x, targetCell.Y + y].Type == CellObjectType.TREE) {
                    return true;
                }
            }
        }

        return false;
    }

    public bool CanToWaterPatch(out List<ErrorType> errors) {
        errors = new List<ErrorType>();

        if (!targetCell.IsPlowed) errors.Add(ErrorType.NOT_IS_PLOWED);
        if (targetCell.IsWatered) errors.Add(ErrorType.IS_WATERED);

        return errors.Count == 0;
    }

    public bool CanCut(out List<ErrorType> errors) {
        errors = new List<ErrorType>();

        if (targetCell.Type != CellObjectType.GRASS) errors.Add(ErrorType.CANT_CUT);
        if (goldController.GetCount() < panelController.SelectedSlot.value.GetPrice()) errors.Add(ErrorType.NOT_ENOUGH_GOLD);

        return errors.Count == 0;
    }

    public bool CanChop(out List<ErrorType> errors) {
        errors = new List<ErrorType>();
        if (targetCell.Type != CellObjectType.TREE) errors.Add(ErrorType.CANT_CHOP);
        if (goldController.GetCount() < panelController.SelectedSlot.value.GetPrice()) errors.Add(ErrorType.NOT_ENOUGH_GOLD);

        return errors.Count == 0;
    }

    public bool CanClean(out List<ErrorType> errors) {
        errors = new List<ErrorType>();

        if (targetCell.Type != CellObjectType.TRASH) errors.Add(ErrorType.CANT_CLEAN);
        if (goldController.GetCount() < panelController.SelectedSlot.value.GetPrice()) errors.Add(ErrorType.NOT_ENOUGH_GOLD);

        return errors.Count == 0;
    }

    public bool CanPlant(ItemType itemType, out List<ErrorType> errors) {
        errors = new List<ErrorType>();

        if (goldController.GetCount() < panelController.SelectedSlot.value.GetPrice()) errors.Add(ErrorType.NOT_ENOUGH_GOLD);
        if (!targetCell.IsPlowed) errors.Add(ErrorType.NOT_IS_PLOWED);
        if (targetCell.PlantObject != null) errors.Add(ErrorType.IS_PLANTED);

        return errors.Count == 0;
    }
}
