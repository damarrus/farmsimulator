﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class PendingAction {
    public Action callback;
    public ItemType itemType;
    public CellController targetCell;

    public PendingAction (Action callback, ItemType itemType, CellController targetCell) {
        this.callback = callback;
        this.itemType = itemType;
        this.targetCell = targetCell;
    }
}

