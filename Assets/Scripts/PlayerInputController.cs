using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputController : MonoBehaviour {
    private Vector3 horizontalDirection = new Vector3(1, 0, -1);
    private Vector3 verticalDirection = new Vector3(1, 0, 1);

    private Ray mouseRay;
    private CellController targetCell;
    [SerializeField] private GameObject target;
    [SerializeField] private TerrainController terrainController;
    [SerializeField] private PanelController panelController;

    public event Action<Vector3> OnMoveInput;
    public event Action<RaycastHit> OnRaycastToCell;
    public event Action OnCellClicked;


    void Start() {

    }

    void Update() {

        // ����� ������ ������
        mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(mouseRay, out hit, 30, ~1)) {
            OnRaycastToCell?.Invoke(hit);
        }

        // ��������� ������ ��� �����
        if (Input.GetMouseButtonDown(0)) {
            OnCellClicked?.Invoke();
        }

        // ����������� ���������
        var axisHorizontal = Input.GetAxis("Horizontal");
        var axisVertical = Input.GetAxis("Vertical");
        if (axisHorizontal == 0 && axisVertical == 0) return;

        var direction = (axisHorizontal * horizontalDirection) + (axisVertical * verticalDirection);
        if (direction.sqrMagnitude > 1) direction = direction.normalized;

        OnMoveInput.Invoke(direction);
    }
}
