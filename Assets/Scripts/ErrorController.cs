using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ErrorController : MonoBehaviour {

    [SerializeField] private TextMeshProUGUI errorsTextField;

    void Start() {
        
    }

    void Update() {
        
    }

    private string GetErrorText(ErrorType errorType) {
        switch (errorType) {
            case ErrorType.IS_PLOWED: return "��� ��������";
            case ErrorType.NOT_IS_PLOWED: return "��������� ��������";
            case ErrorType.IS_WATERED: return "��� ������";
            case ErrorType.NOT_IS_WATERED: return "��������� ������";
            case ErrorType.IS_EMPTY: return "������ ������";
            case ErrorType.NOT_IS_EMPTY: return "���������� ������ ������";
            case ErrorType.IS_PLANTED: return "��� ��������";
            case ErrorType.NOT_IS_PLANTED: return "����� ������ �� ��������";
            case ErrorType.HAS_GROWN: return "��� �������";
            case ErrorType.NOT_HAS_GROWN: return "��� �� �������";
            case ErrorType.NOT_ENOUGH_GOLD: return "����� ������ ������";
            case ErrorType.CANT_CHOP: return "����� ������ ������";
            case ErrorType.CANT_CUT: return "����� ������ �������";
            case ErrorType.CANT_PLANT: return "������ �������� ����� � �������";
            case ErrorType.TOO_FAR: return "������� ������";
            case ErrorType.CAN_COLLECT: return "����� �������";
            case ErrorType.CANT_CLEAN: return "����� ������ �������";
            default: return "����������� ������";
        }
    }

    public void Show(List<ErrorType> errors) {
        string text = "";
        foreach (var error in errors) {
            text += GetErrorText(error) + System.Environment.NewLine;
        }

        errorsTextField.text = text;
    }
    
}