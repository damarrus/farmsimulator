using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstrumentController : MonoBehaviour {
    [SerializeField] private PlayerInputController playerInputController;
    [SerializeField] private PlayerController playerController;
    [SerializeField] private TargetController targetController;
    [SerializeField] private PanelController panelController;
    [SerializeField] private GoldController goldController;

    public event Action<CellController> OnAction;

    void Start() {
        playerInputController.OnCellClicked += CellClickHandler;
    }

    private void CellClickHandler() {
        var targetCell = targetController.targetCell;
        if (targetCell != null && targetController.CanAction) {

            var cellPosition = targetCell.transform.position;

            if (targetController.CanCollect()) {
                playerController.StartAnimation(() => RewardAndAction(targetCell.Collect, targetCell.PlantObject.reward), ItemType.EMPTY, targetCell);
            } else {
                var price = panelController.NextSlot != null ? panelController.NextSlot.value.GetPrice() : panelController.SelectedSlot.value.GetPrice();
                var itemType = panelController.NextSlot != null ? panelController.NextSlot.type : panelController.SelectedSlot.type;

                switch (itemType) {
                    case ItemType.SHOVEL:
                        playerController.StartAnimation(() => PayAndAction(targetCell.Plow, price), itemType, targetCell);
                        break;
                    case ItemType.BUCKET:
                        playerController.StartAnimation(() => PayAndAction(targetCell.ToWaterPatch, price), itemType, targetCell);
                        break;
                    case ItemType.HAND:
                    case ItemType.SICKLE:
                    case ItemType.AXE:
                        playerController.StartAnimation(() => PayAndAction(targetCell.Clean, price), itemType, targetCell);
                        break;
                    case ItemType.CARROT:
                    case ItemType.POTATO:
                    case ItemType.BEET:
                        playerController.StartAnimation(() => PayAndAction(() => targetCell.Plant(itemType), price), itemType, targetCell);
                        break;
                }
            }
        }
    }

    private void PayAndAction(Action action, int price) {
        action();
        goldController.ChangeCount(-price);
    }

    private void RewardAndAction(Action action, int reward) {
        action();
        goldController.ChangeCount(reward);
    }

}

