public enum ItemType {

    NONE = 0,
    SHOVEL = 1,
    BUCKET = 2,
    FERTILIZERS = 3,
    SICKLE = 4,
    AXE = 5,
    CARROT = 6,
    POTATO = 7,
    BEET = 8,
    EMPTY = 9,
    HAND = 10,

}
