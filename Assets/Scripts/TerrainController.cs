using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainController : MonoBehaviour {

    [SerializeField] private new Camera camera;
    [SerializeField] private CellController cellPrefab;
    [SerializeField] private GoldController goldController;
    [SerializeField] private TasksController tasksController;
    [SerializeField] private int fieldSize;

    
    public CellController[,] cells;

    void Start()
    {
        // 0 - ������ ������
        // 1 - ����� �����
        // 2 - ������� �����
        // 3 - ������ �����
        // 4 - ������ �����
        // 5 - �����
        // 6 - ���������� ������
        // 7 - ������������ ������
        // 8 - �������� ������
        // 9 - ������������� ������
        // 10 - �����

                                            // �����
        int[,] configField = {              // |  |                                  // <- ������ ������� ����
            { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 9, 4, 4, 4, 4, 4, 4, 4, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 9, 3, 5, 5, 6, 5, 8, 5, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 9, 3, 8,10, 5,10,10,10, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 9, 3, 5,10, 5, 6, 5, 6, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, // <- �����
            { 0, 0, 0, 0, 9, 3, 5, 7, 5, 5, 5,10, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, // <- �����
            { 0, 0, 0, 0, 9, 3, 5,10,10, 5, 8,10, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 9, 3, 5, 5, 5, 5,10,10, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 9, 3,10,10, 5, 9, 9, 9, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 9, 3,10,10, 0, 9, 9, 9, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 9, 3,10, 0, 0, 9, 9, 9, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 9, 3, 2, 2, 2, 2, 2, 2, 2, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
        };

        // ������������� ������ ��� ��������
        var field = new int[fieldSize + 1, fieldSize + 1];
        for (int i = fieldSize; i >= 0; --i) {
            for (int j = 0; j <= fieldSize; ++j) {
                field[j, fieldSize - i] = configField[i, j];
            }
        }

        cells = new CellController[fieldSize + 1, fieldSize + 1];

        for (int i = -fieldSize; i <= fieldSize; i += 2) {
            for (int j = -fieldSize; j <= fieldSize; j += 2) {
                var newCell = Instantiate(cellPrefab, new Vector3(i, 0, j), cellPrefab.transform.rotation);
                newCell.X = (i + fieldSize) / 2;
                newCell.Y = (j + fieldSize) / 2;
                newCell.name = $"Cell({newCell.X}:{newCell.Y})";
                newCell.Camera = camera;
                cells[newCell.X, newCell.Y] = newCell;

                newCell.OnCollect += tasksController.SetPlantsTasks;

                switch (field[newCell.X, newCell.Y]) {
                    case 1: newCell.SpawnFence(FenceType.LB); break;
                    case 2: newCell.SpawnFence(FenceType.LT); break;
                    case 3: newCell.SpawnFence(FenceType.RT); break;
                    case 4: newCell.SpawnFence(FenceType.RB); break;
                    case 5: newCell.SpawnGrass(); break;
                    case 6: newCell.SpawnTree(); break;
                    case 7: newCell.SpawnOrangeTree(); break;
                    case 8: newCell.SpawnAppleTree(); break;
                    case 9: newCell.SpawnBlock(); break;
                    case 10: newCell.SpawnTrash(); break;
                }
            }
        }

    }

    public CellController GetCell(Vector3 point) {
        var newX = (int)Mathf.Floor((point.x + fieldSize + 1) / 2);
        var newY = (int)Mathf.Floor((point.z + fieldSize + 1) / 2);

        return cells[newX, newY];
    }

    void Update()
    {
        
    }
}
