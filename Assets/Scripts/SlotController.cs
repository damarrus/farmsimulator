using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SlotController : MonoBehaviour, IPointerClickHandler {

    [SerializeField] private Image image;
    [SerializeField] private TextMeshProUGUI count;
    [SerializeField] private GameObject select;
    [SerializeField] private GameObject notAvailable;
    [SerializeField] private int price = 0;

    public event Action OnClick;

    void Start() {
        SetCount(price);
        //SetSelected(false);
    }

    public void OnPointerClick(PointerEventData eventData) {
        OnClick.Invoke();
    }

    public void SetCount(int count) {
        this.count.text = count.ToString();
    }

    public void SetSelected(bool selected) {
        select.SetActive(selected);
    }

    public void SetNotAvailable(bool available) {
        notAvailable.SetActive(available);
    }

    public int GetPrice() {
        return price;
    }
}
