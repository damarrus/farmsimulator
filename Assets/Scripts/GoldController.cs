using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GoldController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI countText;
    [SerializeField] private TextMeshProUGUI countChangeText;
    [SerializeField] private Animator animator;
    [SerializeField] private int count = 100;

    private static int CHANGE = Animator.StringToHash("change");

    public event Action<int> OnChangeGold;

    void Start()
    {
        countText.text = count.ToString();
    }

    void Update()
    {
        
    }

    public int GetCount() {
        return count;
    }

    public void ChangeCount(int delta) { 
        count += delta;
        countText.text = count.ToString();
        OnChangeGold.Invoke(count);
        ShowChangeCount(delta);
    }

    private void ShowChangeCount(int delta) {
        if (delta != 0) {
            var sign = delta > 0 ? "+" : "-";
            countChangeText.text = sign + Mathf.Abs(delta).ToString();
            animator.SetTrigger(CHANGE);
        }
    }
}
