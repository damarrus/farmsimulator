using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellController : MonoBehaviour {
    [SerializeField] private PatchController emptyField;
    [SerializeField] private PlantController carrotField;
    [SerializeField] private PlantController potatoField;
    [SerializeField] private PlantController beetField;
    [SerializeField] private GameObject tree;
    [SerializeField] private GameObject orangeTree;
    [SerializeField] private GameObject appleTree;
    [SerializeField] private GameObject grass;
    [SerializeField] private GameObject fenceLB;
    [SerializeField] private GameObject fenceLT;
    [SerializeField] private GameObject fenceRT;
    [SerializeField] private GameObject fenceRB;

    [SerializeField] private GameObject[] grassArray;
    [SerializeField] private GameObject[] trashArray;

    public int X;
    public int Y;
    public bool IsPlowed = false;
    public bool IsWatered = false;
    public bool IsFertilized = false;
    public CellObjectType Type = CellObjectType.EMPTY;
    public PatchController Patch;
    public Transform PatchCanvasTransform;
    public PlantController PlantObject;
    public GameObject BlockObject;
    public new Camera Camera;

    public event Action<ItemType> OnCollect;

    void Start() {

    }

    void Update() {

    }

    #region ���������

    public void SpawnGrass() {
        var grassType = UnityEngine.Random.Range(0, grassArray.Length);
        var grassPrefab = grassArray[grassType];
        var grassRotationY = UnityEngine.Random.Range(0, 360);
        var grassRotation = Quaternion.Euler(transform.rotation.x, grassRotationY, transform.rotation.z);
        BlockObject = Instantiate(grassPrefab, transform.position, grassRotation);
        Type = CellObjectType.GRASS;
    }

    public void SpawnTrash() {
        var trashType = UnityEngine.Random.Range(0, trashArray.Length);
        var trashPrefab = trashArray[trashType];
        var trashRotationY = UnityEngine.Random.Range(0, 360);
        var trashRotation = Quaternion.Euler(transform.rotation.x, trashRotationY, transform.rotation.z);
        BlockObject = Instantiate(trashPrefab, transform.position, trashRotation);
        Type = CellObjectType.TRASH;
    }

    public void SpawnTree() {
        BlockObject = Instantiate(tree, transform.position, transform.rotation);
        Type = CellObjectType.TREE;
    }

    public void SpawnOrangeTree() {
        BlockObject = Instantiate(orangeTree, transform.position, transform.rotation);
        Type = CellObjectType.TREE;
    }

    public void SpawnAppleTree() {
        BlockObject = Instantiate(appleTree, transform.position, transform.rotation);
        Type = CellObjectType.TREE;
    }

    public void SpawnBlock() {
        Type = CellObjectType.BLOCK;
    }

    public void SpawnFence(FenceType fenceType) {
        GameObject fence = null;
        switch (fenceType) {
            case FenceType.LB: fence = fenceLB; break;
            case FenceType.LT: fence = fenceLT; break;
            case FenceType.RT: fence = fenceRT; break;
            case FenceType.RB: fence = fenceRB; break;
        }

        BlockObject = Instantiate(fence, fence.transform.position + transform.position, fence.transform.rotation);
        Type = CellObjectType.BLOCK;
    }

    #endregion

    public void Clean() {
        Type = CellObjectType.EMPTY;
        Destroy(BlockObject);
    }

    public void Plow() {
        Patch = Instantiate(emptyField, transform.position, transform.rotation);
        Patch.Camera = Camera;
        IsPlowed = true;
    }

    public void ToWaterPatch() {
        Patch.ToWater();

        IsWatered = true;
        Patch.WaterStop(false);

        if (PlantObject != null) {
            PlantObject.StartGrowing();
        } else {
            Patch.PlantStop(true);
        }
    }

    public PlantController GetField(ItemType itemType) {
        switch (itemType) {
            case ItemType.CARROT: return carrotField;
            case ItemType.POTATO: return potatoField;
            case ItemType.BEET: return beetField;
            default: return null;
        }
    }

    public void Plant(ItemType itemType) {

        PlantController field = GetField(itemType);

        if (field != null) {
            PlantObject = Instantiate(field, transform.position, transform.rotation);
            PlantObject.PlantType = itemType;

            PlantObject.OnGrowth += () => Patch.PlantGrowth(true);
            Patch.PlantStop(false);

            if (IsWatered) {
                PlantObject.StartGrowing();
            } else {
                Patch.WaterStop(true);
            }
        }
        
    }

    public void Collect() {
        Patch.PlantGrowth(false);
        Patch.PlantStop(true);
        OnCollect?.Invoke(PlantObject.PlantType);
        Destroy(PlantObject.gameObject);
    }

}
