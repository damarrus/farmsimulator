public enum CellObjectType {

    EMPTY = 1,
    BLOCK = 2,
    GRASS = 3,
    TREE = 4,
    CARROT = 5,
    POTATO = 6,
    BEET = 7,
    TRASH = 8,

}
