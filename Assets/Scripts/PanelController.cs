using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PanelController : MonoBehaviour {

    [SerializeField] private List<KeySlotTuple> map;
    [SerializeField] private GoldController goldController;
    [SerializeField] private PlayerController playerController;

    public KeySlotTuple SelectedSlot;
    [HideInInspector] public KeySlotTuple NextSlot { get; set; }
    public event Action<ItemType> OnSlotChange;

    void Start() {
        NextSlot = null;
        foreach (var item in map) {
            item.value.OnClick += () => SelectItem(item.key);
        }
        SelectItem(KeyCode.Alpha1);
        goldController.OnChangeGold += SetNotAvailable;
        SetNotAvailable(goldController.GetCount());
    }

    private void SetNotAvailable(int goldCount) {
        foreach (var item in map) {
            item.value.SetNotAvailable(item.value.GetPrice() > goldCount);
        }
    }

    void Update() {
        if (!playerController.isBusy && NextSlot != null) {
            SelectItem(NextSlot.key);
            NextSlot = null;
        } else {
            foreach (var item in map) {
                if (Input.GetKeyDown(item.key)) {
                    if (playerController.isBusy) {
                        NextSlot = item;
                    } else {
                        SelectItem(item.key);
                    }
                }
            }
        }
    }

    public SlotController GetSprite(KeyCode key) {
        return map.FirstOrDefault(x => x.key == key)?.value;
    }

    private void SelectItem(KeyCode key) {
        foreach (var item in map) {
            item.value.SetSelected(key == item.key);
            if (key == item.key) {
                SelectedSlot = item;
                OnSlotChange?.Invoke(item.type);
            }
        }
    }

    [Serializable]
    public class KeySlotTuple {
        public KeyCode key;
        public SlotController value;
        public ItemType type;
    }
}
