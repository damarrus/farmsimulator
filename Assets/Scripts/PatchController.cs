using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatchController : MonoBehaviour {
    [SerializeField] private Canvas canvas;
    [SerializeField] private MeshRenderer meshRenderer;
    [SerializeField] private Color wateredFieldColor;
    [SerializeField] private GameObject plantGrowth;
    [SerializeField] private GameObject plantStop;
    [SerializeField] private GameObject waterStop;

    private MaterialPropertyBlock materialPropertyBlock;

    static private Plane horizontalPlane = new Plane(new Vector3(0, 7, 0), new Vector3(1, 7, 1), new Vector3(1, 7, 0));

    public new Camera Camera;

    void Start() {
        materialPropertyBlock = new MaterialPropertyBlock();
        meshRenderer.GetPropertyBlock(materialPropertyBlock);
        plantStop.SetActive(true);
    }

    void Update() {
        var direction = transform.position - Camera.transform.position;
        var ray = new Ray(Camera.transform.position, direction);
        horizontalPlane.Raycast(ray, out float enter);
        canvas.transform.position = Camera.transform.position + direction.normalized * enter;
    }

    public void ToWater() {
        materialPropertyBlock.SetColor("_Color", wateredFieldColor);
        meshRenderer.SetPropertyBlock(materialPropertyBlock);
    }

    public void PlantStop(bool value) {
        plantStop.SetActive(value);
    }
    public void PlantGrowth(bool value) {
        plantGrowth.SetActive(value);
    }
    public void WaterStop(bool value) {
        waterStop.SetActive(value);
    }
}
