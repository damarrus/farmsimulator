public enum ErrorType {

    IS_PLOWED = 1,
    NOT_IS_PLOWED = 2,
    IS_WATERED = 3,
    NOT_IS_WATERED = 4,
    IS_EMPTY = 5,
    NOT_IS_EMPTY = 6,
    IS_PLANTED = 7,
    NOT_IS_PLANTED = 8,
    HAS_GROWN = 9,
    NOT_HAS_GROWN = 10,
    NOT_ENOUGH_GOLD = 11,
    CANT_CHOP = 12,
    CANT_CUT = 13,
    CANT_PLANT = 14,
    TOO_FAR = 15,
    CAN_COLLECT = 16,
    CANT_CLEAN = 17

}
